﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace TwitchClient
{
    public static class Helper
    {
        [Conditional("DEBUG")]
        public static void OutputDebug(string _message, params object[] _params)
        {
            _message = String.Format(_message, _params);
            Console.WriteLine("[DEBUG] {0}", _message);
        }

        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }

        public static bool IsJPEG(string _path)
        {
            if (new FileInfo(_path).Length == 0)
            {
                OutputDebug("File {0}: ZERO", _path);
                return false;
            }

            using (BinaryReader br = new BinaryReader(File.Open(_path, FileMode.Open)))
            {
                UInt16 soi = br.ReadUInt16();  // Start of Image (SOI) marker (FFD8)
                UInt16 jfif = br.ReadUInt16(); // JFIF marker (FFE0) or EXIF marker(FF01)

                bool b = soi == 0xd8ff && (jfif == 0xe0ff || jfif == 57855);

                if (b)
                {
                    OutputDebug("File {0}: JPEG", _path);
                }
                else
                {
                    OutputDebug("File {0}: UNKNOWN", _path);
                }


                return b;
            }
        }

        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("{0} {1} ago",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("{0} {1} ago",
                span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("{0} {1} ago",
                span.Hours, span.Hours == 1 ? "hour" : "hours");
            if (span.Minutes > 0)
                return String.Format("{0} {1} ago",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("{0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return string.Empty;
        }
    }
}
