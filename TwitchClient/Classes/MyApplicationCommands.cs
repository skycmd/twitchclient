﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TwitchClient.Commands
{
    public static class MyApplicationCommands
    {
        public static readonly RoutedUICommand ShowAboutDialog = new RoutedUICommand
        (
                "About",
                "ShowAboutDialog",
                typeof(MyApplicationCommands),
                new InputGestureCollection()
                {
                            new KeyGesture(Key.F1, ModifierKeys.Alt)
                }
        );
        public static readonly RoutedUICommand ShowSettingsDialog = new RoutedUICommand
        (
                "Settings",
                "ShowSettingsDialog",
                typeof(MyApplicationCommands),
                new InputGestureCollection()
                {
                            new KeyGesture(Key.F2, ModifierKeys.Alt)
                }
        );
        public static readonly RoutedUICommand Navigate = new RoutedUICommand
        (
                "Navigate",
                "Navigate",
                typeof(MyApplicationCommands)
        );

    }
}
