﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace TwitchClient.Pages
{
    /// <summary>
    /// Interaction logic for Error.xaml
    /// </summary>
    public partial class Error : Page
    {
        public Error(string errorMessage = "An error has occured", long errCode = 0, bool showReport = true)
        {
            InitializeComponent();

            this.txtError.Text = errorMessage;

            if (!showReport)
                this.txtErrorReport.Visibility = Visibility.Collapsed;

            if (errCode != 0)
                this.txtErrorCode.Text = String.Format("Error Code: E{0}", errCode);
        }

        private void OnUrlClick(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            var link = (Hyperlink)sender;
            // Do something with link.NavigateUri like:
            Process.Start(link.NavigateUri.ToString());
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}