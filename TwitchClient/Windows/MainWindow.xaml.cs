﻿using System;
using System.Windows;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Threading;


namespace TwitchClient.Windows
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DebugFlagEnabled();

            TextBoxHelper.SetWatermark(txtChannelName, "Load channel");
        }

        [Conditional("DEBUG")]
        public void DebugFlagEnabled()
        {
            Title = String.Concat("[DEBUG] ", Title);
        }


        

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ShowAboutDialog_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !Helper.IsWindowOpen<Windows.About>();
        }

        private void ShowAboutDialog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Windows.About w = new Windows.About();
            w.Show();
        }

        private void ShowSettingsDialog_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !Helper.IsWindowOpen<Windows.Settings>();
        }

        private void ShowSettingsDialog_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Windows.Settings w = new Windows.Settings();
            w.Show();
        }

        private void btnDirectoryFeatured_Click(object sender, RoutedEventArgs e)
        {
            //Pages.Directory d = new Pages.Directory(Pages.Directory.Criteria.Following, tHandlerInstance.TwitchClient);
            //_mainFrame.Navigate(d);
        }

        private void btnDirectoryFollowing_Click(object sender, RoutedEventArgs e)
        {
            //Pages.Directory d = new Pages.Directory(Pages.Directory.Criteria.Following, tHandlerInstance.TwitchClient);
            //_mainFrame.Navigate(d);
        }

        private void btnDirectoryGames_Click(object sender, RoutedEventArgs e)
        {
            //Pages.Directory d = new Pages.Directory(Pages.Directory.Criteria.Games, tHandlerInstance.TwitchClient);
            //_mainFrame.Navigate(d);
        }

        private void btnDirectorChannels_Click(object sender, RoutedEventArgs e)
        {
            //Pages.Directory d = new Pages.Directory(Pages.Directory.Criteria.Channels, tHandlerInstance.TwitchClient);
            //_mainFrame.Navigate(d);
        }

        private void btnDirectoryVideos_Click(object sender, RoutedEventArgs e)
        {
            //Pages.Directory d = new Pages.Directory(Pages.Directory.Criteria.Videos, tHandlerInstance.TwitchClient);
            //_mainFrame.Navigate(d);
        }

        private void pageRedirect(object sender, EventArgs e)
        {
            //ctcContent.Content = (System.Windows.Controls.Page) e.Content;
        }

        private void _mainFrame_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            Console.WriteLine("Navigating");
            //pgrNavigating.Visibility = Visibility.Visible;
            _mainFrame.Visibility = Visibility.Collapsed;
        }

        private void _mainFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Console.WriteLine("Navigated");
        }

        private void _mainFrame_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Console.WriteLine("LoadCompleted");
            //pgrNavigating.Visibility = Visibility.Collapsed;
            _mainFrame.Visibility = Visibility.Visible;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshMaximiseIconState();

            Pages.Featured d = new Pages.Featured();
            _mainFrame.Navigate(d);
        }

        #region "Window Control"
        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                ToggleMaximized();
            }
        }

        private void btnMinimise_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaxRestore_Click(object sender, RoutedEventArgs e)
        {
            ToggleMaximized();
        }

        private void ToggleMaximized()
        {
            WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        protected override void OnStateChanged(System.EventArgs e)
        {
            RefreshMaximiseIconState();
            base.OnStateChanged(e);
        }

        void RefreshMaximiseIconState()
        {
            if (WindowState == WindowState.Normal)
            {
                btnWinMaxRestore.Content = "1";
            }
            else
            {
                btnWinMaxRestore.Content = "2";
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        #endregion
    }
}