﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Diagnostics;

namespace TwitchClient.Windows
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : MetroWindow
    {
        public About()
        {
            InitializeComponent();

            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersion.Content = String.Format("Version {0}", version);

            // tbkAck.Text = Properties.Resources.Authors;
        }

        private void btnSystemInfo_Click(object sender, RoutedEventArgs e)
        {
            // Launch SystemInformation
            Process.Start("msinfo32.exe");
        }

        private void btnDxDiag_Click(object sender, RoutedEventArgs e)
        {
            // Launch DxDiag
            Process.Start("dxdiag.exe");
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            // Close this window
            this.Close();
        }

        private void btnDonate_Click(object sender, RoutedEventArgs e)
        {
            // Launch PayPal
            Process.Start("http://");
        }
    }
}
